import { Grid } from '@mui/material'
import React from 'react'
import {useSelector,useDispatch} from 'react-redux';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import './homeStyle.css'
import { Link } from 'react-router-dom';
import { logOut } from '../../store/slices/usuarios';
import TableHome from '../../components/table/tableHome';



const Home = () => {

    const { usuario } = useSelector( state => state.user );
  
    const dispatch = useDispatch()
    const logout = ()=>{
        dispatch(logOut())
        localStorage.removeItem('user');
    }

  return (
    <Grid container className='homeContainer'  >
        <Grid container justifyContent={'space-between'} alignItems={'center'}>
            <Grid item xs={4}>
               <p className='text'> {`Hola ${usuario?.name} `}</p>
            </Grid>
            <Grid item xs={4} display={'flex'} alignItems={'center'} justifyContent={'end'}>
                <AccountCircleIcon fontSize='large'  style={{color:'#F43D3D'}}/>
                <Link className='text logout' onClick={logout}  >Logout</Link>
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <p className='titleTable' >Dashboard</p>
        </Grid>
        <TableHome />
       
    </Grid>
  )
}

export default Home
