import React from 'react';
import LoginForm from '../../components/loginForm/LoginForm';
import { Grid } from '@mui/material';

const Login = () => {
  return (
    <Grid container justifyContent="end" alignItems="center" style={{ height: '100vh',paddingRight:100 }}>
      <Grid item xs={12} md={4}>
        <LoginForm />
      </Grid>
    </Grid>
  );
};

export default Login;

