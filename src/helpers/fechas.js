const nombresMeses = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];

export const formatearDate= (fecha)=>{
    const nuevaFecha = new Date(fecha);
    return `${nombresMeses[nuevaFecha.getMonth()]} ${ nuevaFecha.getDate()}, ${nuevaFecha.getFullYear()}`
}