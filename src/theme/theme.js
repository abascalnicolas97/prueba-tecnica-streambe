import { createTheme } from '@mui/material';
import { red } from '@mui/material/colors';

export const purpleTheme = createTheme({
    palette: {
        primary: {
            main: '#CFCDCD'
        },
        secondary: {
            main: '#F43D3D'
        },
        error: {
            main: red.A400
        }
    }
})