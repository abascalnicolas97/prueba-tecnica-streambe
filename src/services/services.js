import axios from "axios"

export const getUsuarios = async () => {
    const { data } = await axios.get('https://www.mockachino.com/06c67c77-18c4-45/users')
    return data
}

export const userLogin = async () => {
    const { data } = await axios.get('https://www.mockachino.com/06c67c77-18c4-45/login')
    return data
}