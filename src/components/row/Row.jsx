import { Grid } from '@mui/material'
import React, { useState } from 'react'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import './row.css'
const Row = ({name,surnames,birthDate,photo}) => {
    const [imgError,setImgError]=useState(false)
  return (
    <Grid container className='rowTable'>
          <Grid item xs={5} sm={6}  display={'flex'} justifyContent={'center'} alignItems={'center'}>
                    <p className=''>{`${name} ${surnames}`}</p>
                </Grid>
                <Grid item  xs={5}  display={'flex'} justifyContent={'center'} alignItems={'center'}className='column'>
                    <p className=''>{birthDate}</p>
                </Grid>
                <Grid item xs={2} sm={1}  display={'flex'}  justifyContent={'center'} alignItems={'center'}>
                    {
                        photo && !imgError?<img style={{maxWidth: '100% ',height:100}} src={photo} onError={()=>setImgError(true)} alt="photo" />:
                        <AccountCircleIcon fontSize='large'  style={{color:'#F43D3D'}}/>
                    }
                </Grid>
    </Grid>
  )
}

export default Row
