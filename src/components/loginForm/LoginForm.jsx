import React, { useState } from 'react'
import { useForm } from '../../hook/useForm';
import { useDispatch } from 'react-redux';
import { Alert, Button, Grid, TextField } from '@mui/material';
import { getUser } from '../../store/slices/thunks';
import { useNavigate } from 'react-router-dom';




const LoginForm = () => {
    const [errorMessage ,setErrorMessage] = useState(null);
    const navigation = useNavigate();
    const dispatch = useDispatch();
    const formValidations = {
      username:[(value)=>value.length > 0,'este campo es requerido'],
      password:[(value)=>value.length > 0,'este campo es requerido']
    }
    const { username, password,passwordValid,usernameValid, onInputChange,createValidators } = useForm({
        username: '',
        password: ''
      },formValidations);

    const onSubmit= (event)=>{
      
        event.preventDefault();
        
        if(createValidators()){
          dispatch(getUser(setErrorMessage));
          navigation('/');
        }
               
    }
    
  return (
    
    <form onSubmit={ onSubmit } >
        <Grid container display='flex' justifyContent={'center'} alignItems={'center'} >
          <h4 style={{fontSize:40}}  >Login</h4>
        <Grid item xs={ 12 } sx={{ mt: 2 }}>
              <TextField
                label="Username" 
                error={!!usernameValid}
                helperText={usernameValid}
                placeholder='username' 
                fullWidth
                name="username"
                value={ username }
                onChange={ onInputChange }
              />
            </Grid>
            <Grid item xs={ 12 } sx={{ mt: 2 }}>
              <TextField
                label="Password" 
                error={!!passwordValid}
                helperText={passwordValid}
                placeholder='Password' 
                fullWidth
                name="password"
                value={ password }
                onChange={ onInputChange }
              />
            </Grid>
            <Grid 
              container
              display={ !!errorMessage ? '': 'none' }
              sx={{ mt: 1 }}>
              <Grid 
                  item 
                  xs={ 12 }
                >
                <Alert severity='error'>{ errorMessage }</Alert>
              </Grid>
            </Grid>
            <Grid item xs={ 12 } sm={ 12 } sx={{ mt: 2 }}>
                <Button
                  style={{backgroundColor:'#F43D3D',color:'white'}}
                  type="submit" 
                  variant='contained' 
                  fullWidth>
                  Login
                </Button>
              </Grid>
        </Grid>
    </form>

  )
}

export default LoginForm
