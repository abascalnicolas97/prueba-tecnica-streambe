import { createSlice } from "@reduxjs/toolkit";

const  userState = {
    usuario:null
}

const userSlice = createSlice({
    name:'user',
    initialState:userState,
    reducers:{
        setUser: (state,action) =>{
            state.usuario = action.payload
        },
        logOut: (state)=>{
            state.usuario=null
        }
    }
}) 

export const {setUser,logOut}= userSlice.actions
export default userSlice