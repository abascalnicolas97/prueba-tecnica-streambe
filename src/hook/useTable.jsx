import React, { useEffect, useState } from 'react'
import { getUsuarios } from '../services/services'
import { formatearDate } from '../helpers/fechas'

const useTable = () => {
    const [datos,setDatos] = useState([])
    

    useEffect(() => {
        getUsers()
    }, [])

    const getUsers = async () =>{
       const data = await getUsuarios()
       
        const dataFormateada = data.users?.map(elem => {
            elem.birthDate = formatearDate(elem.birthDate);
            
            return elem;
          });
        setDatos(dataFormateada)
       
    }

 
    
  return datos
}

export default useTable
